#include <fftw3.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// simuC 0.1 0.27 0.001 1.0 11 140 1024 5 5 20000 0.001 6

int main(int argc, const char * argv[]) {
    const double L=atof(argv[6]); 
    const double N=atof(argv[7]);
    const double alpha=atof(argv[1]);
    const double lb=atof(argv[2]);
    const double kappa=atof(argv[3]);
    const double D0=atof(argv[4]);
    const double u0=atof(argv[5]);
    const double Nt=atof(argv[8])*pow(10,atol(argv[9]));
    const double skip=atof(argv[10]);
    const double Dt=atof(argv[11]);
    const double sigma=atof(argv[12]);
    const double dx=L/N;
    double Unit =1;
    const double pi=4*atan(Unit);

    double max, min, t;

    int i,k;
    printf("%f \n",Nt);

    double *U1,*U2,*U3,*U4,*Ut,*u,*nablaDU,*nablaU,*nabla4,*Dunu, *DU, *x, *xi, *xi4 ;
    fftw_complex *uh, *DUH,*nablaUH,*nabla4h,*nablaDUH;

    fftw_plan planf,planfD, planb,planb4, planbD;
    u=(double *) fftw_malloc(sizeof(double) * N);
    nablaDU=(double *) fftw_malloc(sizeof(double) * N);
    nablaU=(double *) fftw_malloc(sizeof(double) * N);
    nabla4=(double *) fftw_malloc(sizeof(double) * N);
    Dunu=(double *) fftw_malloc(sizeof(double) * N);
    U1=(double *) fftw_malloc(sizeof(double) * N);
    U2=(double *) fftw_malloc(sizeof(double) * N);
    U3=(double *) fftw_malloc(sizeof(double) * N); 
    U4=(double *) fftw_malloc(sizeof(double) * N); 
    Ut=(double *) fftw_malloc(sizeof(double) * N); 
    DU=(double *) fftw_malloc(sizeof(double) * N); 
    x=(double *) fftw_malloc(sizeof(double) * N);
    xi=(double *) fftw_malloc(sizeof(double) * (N/2+1));
    xi4=(double *) fftw_malloc(sizeof(double) * (N/2+1));

    uh=(fftw_complex *) fftw_malloc(sizeof(fftw_complex) * (N/2+1));
    nablaDUH=(fftw_complex *) fftw_malloc(sizeof(fftw_complex) * (N/2+1));
    nablaUH=(fftw_complex *) fftw_malloc(sizeof(fftw_complex) * (N/2+1));
    nabla4h=(fftw_complex *) fftw_malloc(sizeof(fftw_complex) * (N/2+1));
    DUH=(fftw_complex *) fftw_malloc(sizeof(fftw_complex) * (N/2+1));

    planf =fftw_plan_dft_r2c_1d(N, Ut, uh,FFTW_MEASURE | FFTW_DESTROY_INPUT);
    planfD =fftw_plan_dft_r2c_1d(N, Dunu, DUH,FFTW_MEASURE | FFTW_DESTROY_INPUT);
    planb =fftw_plan_dft_c2r_1d(N, nablaUH, nablaU, FFTW_MEASURE | FFTW_DESTROY_INPUT);
    planb4 =fftw_plan_dft_c2r_1d(N, nabla4h, nabla4, FFTW_MEASURE | FFTW_DESTROY_INPUT);
    planbD =fftw_plan_dft_c2r_1d(N, nablaDUH, nablaDU, FFTW_MEASURE | FFTW_DESTROY_INPUT);

    for (i = 0; i < N; i++)
    {
        x[i]=-L/2+dx*i;
    }

    for (i = 0; i < N/2+1; i++)
    {
        xi[i]=2*pi/L*i;
        xi4[i]=pow(2*pi/L*i,4);
    }
    max=0;
    for (i = 0; i < N; i++)
    {
        u[i]=exp(-pow(x[i],2)/(2*pow(sigma,2)));
        if (u[i]>max)
        {
            max=u[i];
        }
        
    }
    for (i = 0; i < N; i++)
    {
        u[i]=u[i]/max*1.1*u0;
    }
    
    for (k = 0; k < Nt; k++)
    {
        // iteration 1
        for (i = 0; i < N; i++)
        {
            Ut[i]=u[i];
            DU[i]=D0*exp(-lb*Ut[i])*(1-lb*Ut[i]/2);
            printf("%f",u[i]);
        }
        fftw_execute(planf);
        for (i = 0; i < N/2+1; i++)
        {
            nablaUH[i][0]=-xi[i]*uh[i][1];
            nablaUH[i][1]=xi[i]*uh[i][0];
            nabla4h[i][0]=xi4[i]*uh[i][0];
            nabla4h[i][1]=xi4[i]*uh[i][1];
        }
        fftw_execute(planb);
        fftw_execute(planb4);
        for (i = 0; i < N; i++)
        {
            Dunu[i]=DU[i]*nablaU[i]*1/N;
        }
        fftw_execute(planfD);
        for (i = 0; i < N/2+1; i++)
        {
            nablaDUH[i][0]=-xi[i]*DUH[i][1];
            nablaDUH[i][1]=xi[i]*DUH[i][0];
        }
        fftw_execute(planbD);
        for (i = 0; i < N; i++)
        {
            U1[i]=Dt*(nablaDU[i]*1/N+alpha*Ut[i]*(1-Ut[i]/u0)-kappa*nabla4[i]*1/N);
        }
        // iteration 2
        for (i = 0; i < N; i++)
        {
            Ut[i]=u[i]+U1[i]/2;
            DU[i]=D0*exp(-lb*Ut[i])*(1-lb*Ut[i]/2);
        }
        fftw_execute(planf);
        for (i = 0; i < N/2+1; i++)
        {
            nablaUH[i][0]=-xi[i]*uh[i][1];
            nablaUH[i][1]=xi[i]*uh[i][0];
            nabla4h[i][0]=xi4[i]*uh[i][0];
            nabla4h[i][1]=xi4[i]*uh[i][1];
        }
        fftw_execute(planb);
        fftw_execute(planb4);
        for (i = 0; i < N; i++)
        {
            Dunu[i]=DU[i]*nablaU[i]*1/N;
        }
        fftw_execute(planfD);
        for (i = 0; i < N/2+1; i++)
        {
            nablaDUH[i][0]=-xi[i]*DUH[i][1];
            nablaDUH[i][1]=xi[i]*DUH[i][0];
        }
        fftw_execute(planbD);
        for (i = 0; i < N; i++)
        {
            U2[i]=Dt*(nablaDU[i]*1/N+alpha*Ut[i]*(1-Ut[i]/u0)-kappa*nabla4[i]*1/N);
        }
        // iteration 3
        for (i = 0; i < N; i++)
        {
            Ut[i]=u[i]+U2[i]/2;
            DU[i]=D0*exp(-lb*Ut[i])*(1-lb*Ut[i]/2);
        }
        fftw_execute(planf);
        for (i = 0; i < N/2+1; i++)
        {
            nablaUH[i][0]=-xi[i]*uh[i][1];
            nablaUH[i][1]=xi[i]*uh[i][0];
            nabla4h[i][0]=xi4[i]*uh[i][0];
            nabla4h[i][1]=xi4[i]*uh[i][1];
        }
        fftw_execute(planb);
        fftw_execute(planb4);
        for (i = 0; i < N; i++)
        {
            Dunu[i]=DU[i]*nablaU[i]*1/N;
        }
        fftw_execute(planfD);
        for (i = 0; i < N/2+1; i++)
        {
            nablaDUH[i][0]=-xi[i]*DUH[i][1];
            nablaDUH[i][1]=xi[i]*DUH[i][0];
        }
        fftw_execute(planbD);
        for (i = 0; i < N; i++)
        {
            U3[i]=Dt*(nablaDU[i]*1/N+alpha*Ut[i]*(1-Ut[i]/u0)-kappa*nabla4[i]*1/N);
        }
        // iteration 4
        for (i = 0; i < N; i++)
        {
            Ut[i]=u[i]+U3[i];
            DU[i]=D0*exp(-lb*Ut[i])*(1-lb*Ut[i]/2);
        }
        fftw_execute(planf);
        for (i = 0; i < N/2+1; i++)
        {
            nablaUH[i][0]=-xi[i]*uh[i][1];
            nablaUH[i][1]=xi[i]*uh[i][0];
            nabla4h[i][0]=xi4[i]*uh[i][0];
            nabla4h[i][1]=xi4[i]*uh[i][1];
        }
        fftw_execute(planb);
        fftw_execute(planb4);
        for (i = 0; i < N; i++)
        {
            Dunu[i]=DU[i]*nablaU[i]*1/N;
        }
        fftw_execute(planfD);
        for (i = 0; i < N/2+1; i++)
        {
            nablaDUH[i][0]=-xi[i]*DUH[i][1];
            nablaDUH[i][1]=xi[i]*DUH[i][0];
        }
        fftw_execute(planbD);
        for (i = 0; i < N; i++)
        {
            U4[i]=Dt*(nablaDU[i]*1/N+alpha*Ut[i]*(1-Ut[i]/u0)-kappa*nabla4[i]*1/N);
            u[i]=U1[i]/6+U2[i]/3+U3[i]/3+U4[i]/6;
        }
        t=t+Dt;
        if (ceil(k/skip)==k/skip)
        {
            max=0;
            min=u0;
            for (i = 0; i < N; i++)
            {
                if (u[i]>max)
                {
                 max=u[i];
                }
                if (u[i]<min)
                {
                 min=u[i];
                }
            }
            printf("min %f", min);
            printf("     max %f \n", max);
        }
        
    }
    
    return 0;
}