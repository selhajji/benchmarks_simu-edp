

PROGRAM simu
use, intrinsic :: iso_c_binding
IMPLICIT NONE
include 'fftw3.f03'
include 'fftw3l.f03'
integer, parameter :: N=4, Nt=10**4
double precision, parameter :: lb=0.27, kappa=0.001 , D0=1, alpha=0.1
double precision, parameter ::L=150, u0=11, Dt=0.001,sigma=5, skip=1000
double precision, parameter :: dx=L/N

double precision, dimension(N) :: x
double precision, dimension(N/2+1) :: xi

double precision, parameter :: pi= 4*atan (1.0_16)
double complex, parameter :: j =(0.0,1.0)
real(C_LONG_DOUBLE), pointer :: U1(:),U2(:),U3(:),U4(:),Ut(:)
double precision :: t=0
real(C_LONG_DOUBLE), pointer :: u(:),nablaDU(:),nablaU(:),nabla4(:),DU(:), Dunu(:)
complex(C_LONG_DOUBLE_COMPLEX), pointer :: uh(:), DUH(:),nablaUH(:),nabla4h(:),nablaDUH(:)
    
integer :: i, k
type(C_PTR) :: planf,planfD, planb,planb4,planbD, pU1,pU2,pU3,pU4,pUt,pu,pnablaDU,pnablaU,pnabla4,pDU, pDunu
type(C_PTR) :: puh, pDUH, pnablaUH, pnabla4h, pnablaDUH

pU1=fftw_alloc_real(int(N,C_SIZE_T))
pU2=fftw_alloc_real(int(N,C_SIZE_T))
pU3=fftw_alloc_real(int(N,C_SIZE_T))
pU4=fftw_alloc_real(int(N,C_SIZE_T))
pUt=fftw_alloc_real(int(N,C_SIZE_T))
pu=fftw_alloc_real(int(N,C_SIZE_T))
pnablaDU=fftw_alloc_real(int(N,C_SIZE_T))
pnablaU=fftw_alloc_real(int(N,C_SIZE_T))
pnabla4=fftw_alloc_real(int(N,C_SIZE_T))
pDU=fftw_alloc_real(int(N,C_SIZE_T))
pDunu=fftw_alloc_real(int(N,C_SIZE_T))
puh=fftw_alloc_complex(int(N/2+1,C_SIZE_T))
pDUH=fftw_alloc_complex(int(N/2+1,C_SIZE_T))
pnablaUH=fftw_alloc_complex(int(N/2+1,C_SIZE_T))
pnabla4h=fftw_alloc_complex(int(N/2+1,C_SIZE_T))
pnablaDUH=fftw_alloc_complex(int(N/2+1,C_SIZE_T))
call c_f_pointer(pU1,U1,[N])
call c_f_pointer(pU2,U2,[N])
call c_f_pointer(pU3,U3,[N])
call c_f_pointer(pU4,U4,[N])
call c_f_pointer(pUt,Ut,[N])
call c_f_pointer(pu,u,[N])
call c_f_pointer(pnablaDU,nablaDU,[N])
call c_f_pointer(pnablaU,nablaU,[N])
call c_f_pointer(pnabla4,nabla4,[N])
call c_f_pointer(pDU,DU,[N])
call c_f_pointer(pDunu,Dunu,[N])
call c_f_pointer(puh,uh,[N/2+1])
call c_f_pointer(pDUH,DUH,[N/2+1])
call c_f_pointer(pnablaUH,nablaUH,[N/2+1])
call c_f_pointer(pnabla4h,nabla4h,[N/2+1])
call c_f_pointer(pnablaDUH,nablaDUH,[N/2+1])

planf =fftwl_plan_dft_r2c_1d(N, Ut, uh,FFTW_MEASURE+FFTW_DESTROY_INPUT)
planfD =fftwl_plan_dft_r2c_1d(N, Dunu, DUH,FFTW_MEASURE+FFTW_DESTROY_INPUT)
planb =fftwl_plan_dft_c2r_1d(N, nablaUH, nablaU,FFTW_MEASURE+FFTW_DESTROY_INPUT)
planb4 =fftwl_plan_dft_c2r_1d(N, nabla4h, nabla4,FFTW_MEASURE+FFTW_DESTROY_INPUT)
planbD =fftwl_plan_dft_c2r_1d(N, nablaDUH, nablaDU,FFTW_MEASURE+FFTW_DESTROY_INPUT)

do i=1,N
    x(i)=-L/2+dx*i
enddo

do i=1,N/2+1
    xi(i)=2*pi/L*i
enddo


u=exp(-x**2/(2*sigma**2))
u=u/maxval(u)*1.1*u0
!write(*,*) minval(u),maxval(u)
do k=1,Nt
    !iteration 1
    Ut=u
    DU=D0*exp(-lb*Ut)*(1-lb*Ut/2)
    call fftwl_execute_dft_r2c(planf, Ut, uh)
    nablaUH=j*xi*uh
    call fftwl_execute_dft_c2r(planb,nablaUH , nablaU)
    nabla4h=xi**4*uh
    call fftwl_execute_dft_c2r(planb4,nabla4h , nabla4)
    Dunu=DU*nablaU*1/N
    call fftwl_execute_dft_r2c(planfD,Dunu , DUH)
    nablaDUH=j*xi*DUH
    call fftwl_execute_dft_c2r(planbD, nablaDUH, nablaDU)
    U1=Dt*(nablaDU*1/N+alpha*Ut*(1-Ut/u0)-kappa*nabla4*1/N)
    !write(*,*) minval(U1),maxval(U1)
    !iteration 2
    Ut=u+U1/2
    DU=D0*exp(-lb*Ut)*(1-lb*Ut/2)
    call fftwl_execute_dft_r2c(planf, Ut, uh)
    nablaUH=j*xi*uh
    call fftwl_execute_dft_c2r(planb,nablaUH , nablaU)
    nabla4h=xi**4*uh
    call fftwl_execute_dft_c2r(planb4,nabla4h , nabla4)
    Dunu=DU*nablaU*1/N
    call fftwl_execute_dft_r2c(planfD,Dunu , DUH)
    nablaDUH=j*xi*DUH
    call fftwl_execute_dft_c2r(planbD, nablaDUH, nablaDU)
    U2=Dt*(nablaDU*1/N+alpha*Ut*(1-Ut/u0)-kappa*nabla4*1/N)
    !iteration 3
    Ut=u+U2/2
    DU=D0*exp(-lb*Ut)*(1-lb*Ut/2)
    call fftwl_execute_dft_r2c(planf, Ut, uh)
    nablaUH=j*xi*uh
    call fftwl_execute_dft_c2r(planb,nablaUH , nablaU)
    nabla4h=xi**4*uh
    call fftwl_execute_dft_c2r(planb4,nabla4h , nabla4)
    Dunu=DU*nablaU*1/N
    call fftwl_execute_dft_r2c(planfD,Dunu , DUH)
    nablaDUH=j*xi*DUH
    call fftwl_execute_dft_c2r(planbD, nablaDUH, nablaDU)
    U3=Dt*(nablaDU*1/N+alpha*Ut*(1-Ut/u0)-kappa*nabla4*1/N)
    !iteration 4
    Ut=u+U3
    DU=D0*exp(-lb*Ut)*(1-lb*Ut/2)
    call fftwl_execute_dft_r2c(planf, Ut, uh)
    nablaUH=j*xi*uh
    call fftwl_execute_dft_c2r(planb,nablaUH , nablaU)
    nabla4h=xi**4*uh
    call fftwl_execute_dft_c2r(planb4,nabla4h , nabla4)
    Dunu=DU*nablaU*1/N
    call fftwl_execute_dft_r2c(planfD,Dunu , DUH)
    nablaDUH=j*xi*DUH
    call fftwl_execute_dft_c2r(planbD, nablaDUH, nablaDU)
    U4=Dt*(nablaDU*1/N+alpha*Ut*(1-Ut/u0)-kappa*nabla4*1/N)
    u=u+U1/6+U2/3+U3/3+U4/6
    t=t+Dt
    write(*,*) t
    if (int(k/skip)==k/skip) then
        write(*,*) minval(u),maxval(u), k
    endif
enddo

END PROGRAM
