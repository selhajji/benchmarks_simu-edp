import numpy as np
import numpy.fft as f
import time

T=[0]
t=0
L=140
N=1024

x=np.linspace(-L/2,L/2,N)
xi=f.rfftfreq(N)*N*2*np.pi/L
filtre = np.ones_like(xi)

filtre[np.where(np.abs(xi)>np.max(xi)*2./3)] = 0.
def nabla(u):
    uhat=f.rfft(u)
    Duhat=1j*xi*uhat
    return f.irfft(Duhat)

def nabla2(u):
    A=time.time()
    uhat=f.rfft(u)
    Duhat=1j*xi*uhat
    du=f.irfft(Duhat)
    return time.time()-A

def nabla4(u):
    uhat=f.rfft(u)
    D4uhat=(1j*xi)**4*uhat
    return f.irfft(D4uhat)


lb=0.27
kappa=10**-3
D0=1
alpha=0.1
u0=11
phi=lb*u0/2
R=D0/np.sqrt(alpha*kappa)

def D(u):
    return D0*np.exp(-lb*u)*(1-lb*u/2)
qc=np.sqrt(2*alpha/abs(D(u0)))

sigma=6
u=1/np.sqrt(2*np.pi*sigma**2)*np.exp(-x**2/(2*sigma**2))

u*=1/np.max(u)*1.1*u0
frames=[u.copy()]
def RKU(u):
    return nabla(D(u)*nabla(u))+alpha*u*(1-u/u0)-kappa*nabla4(u)

skip=20000
Dt=0.001

A=time.time()

for k in range(5*10**5):
    U1=Dt*RKU(u)
    U2=Dt*RKU(u+U1/2)
    U3=Dt*RKU(u+U2/2)
    U4=Dt*RKU(u+U3)
    u+=U1/6+U2/3+U3/3+U4/6
    t+=Dt
    u=f.irfft(f.rfft(u)*filtre)
    u=u*(u>10**-8)
    if np.isnan(np.sum(u)):
        break
    if int(k/skip)==k/skip:
        print(np.min(u),np.max(u))

B=time.time()
print(B-A)