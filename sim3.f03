

PROGRAM simu
use, intrinsic :: iso_c_binding
IMPLICIT NONE
include 'fftw3.f03'
include 'fftw3l.f03'

real :: start, finish

integer, parameter :: N=1024, Nt=10**5
double precision, parameter :: lb=0.27, kappa=0.001 , D0=1, alpha=0.1
double precision, parameter ::L=150, u0=11, Dt=0.001,sigma=5, skip=1000
double precision, parameter :: dx=L/N

double precision, dimension(N) :: x
double precision, dimension(N/2+1) :: xi

double precision, parameter :: pi= 4*atan (1.0_16)
double complex, parameter :: j =(0.0,1.0)
real(C_LONG_DOUBLE), dimension(N) :: U1,U2,U3,U4,Ut
double precision :: t=0
real(C_LONG_DOUBLE), dimension(N) :: u,nablaDU,nablaU,nabla4,DU, Dunu,mu
complex(C_LONG_DOUBLE_COMPLEX), dimension(N/2+1) :: uh, DUH,nablaUH,nabla4h,nablaDUH
    
integer :: i, k
type(C_PTR) :: planf, planb
planf =fftwl_plan_dft_r2c_1d(N, u,uh,FFTW_MEASURE+FFTW_DESTROY_INPUT) !,FFTW_PRESERVE_INPUT
planb =fftwl_plan_dft_c2r_1d(N, uh,u,FFTW_MEASURE+FFTW_DESTROY_INPUT)
do i=1,N
    x(i)=-L/2+dx*i
enddo

do i=1,N/2+1
    xi(i)=2*pi/L*i
enddo


u=exp(-x**2/(2*sigma**2))
u=u/maxval(u)*1.1*u0
!write(*,*) minval(u),maxval(u)
call cpu_time(start)
do k=1,Nt
    !iteration 1
    Ut=u
    DU=D0*exp(-lb*Ut)*(1-lb*Ut/2)
    call fftwl_execute_dft_r2c(planf, Ut, uh)
    nablaUH=j*xi*uh
    call fftwl_execute_dft_c2r(planb,nablaUH , nablaU)
    nabla4h=xi**4*uh
    call fftwl_execute_dft_c2r(planb,nabla4h , nabla4)
    Dunu=DU*nablaU*1/N
    call fftwl_execute_dft_r2c(planf,Dunu , DUH)
    nablaDUH=j*xi*DUH
    call fftwl_execute_dft_c2r(planb, nablaDUH, nablaDU)
    U1=Dt*(nablaDU*1/N+alpha*Ut*(1-Ut/u0)-kappa*nabla4*1/N)
    !write(*,*) minval(U1),maxval(U1)
    !iteration 2
    Ut=u+U1/2
    DU=D0*exp(-lb*Ut)*(1-lb*Ut/2)
    call fftwl_execute_dft_r2c(planf, Ut, uh)
    nablaUH=j*xi*uh
    call fftwl_execute_dft_c2r(planb,nablaUH , nablaU)
    nabla4h=xi**4*uh
    call fftwl_execute_dft_c2r(planb,nabla4h , nabla4)
    Dunu=DU*nablaU*1/N
    call fftwl_execute_dft_r2c(planf,Dunu , DUH)
    nablaDUH=j*xi*DUH
    call fftwl_execute_dft_c2r(planb, nablaDUH, nablaDU)
    U2=Dt*(nablaDU*1/N+alpha*Ut*(1-Ut/u0)-kappa*nabla4*1/N)
    !iteration 3
    Ut=u+U2/2
    DU=D0*exp(-lb*Ut)*(1-lb*Ut/2)
    call fftwl_execute_dft_r2c(planf, Ut, uh)
    nablaUH=j*xi*uh
    call fftwl_execute_dft_c2r(planb,nablaUH , nablaU)
    nabla4h=xi**4*uh
    call fftwl_execute_dft_c2r(planb,nabla4h , nabla4)
    Dunu=DU*nablaU*1/N
    call fftwl_execute_dft_r2c(planf,Dunu , DUH)
    nablaDUH=j*xi*DUH
    call fftwl_execute_dft_c2r(planb, nablaDUH, nablaDU)
    U3=Dt*(nablaDU*1/N+alpha*Ut*(1-Ut/u0)-kappa*nabla4*1/N)
    !iteration 4
    Ut=u+U3
    DU=D0*exp(-lb*Ut)*(1-lb*Ut/2)
    call fftwl_execute_dft_r2c(planf, Ut, uh)
    nablaUH=j*xi*uh
    call fftwl_execute_dft_c2r(planb,nablaUH , nablaU)
    nabla4h=xi**4*uh
    call fftwl_execute_dft_c2r(planb,nabla4h , nabla4)
    Dunu=DU*nablaU*1/N
    call fftwl_execute_dft_r2c(planf,Dunu , DUH)
    nablaDUH=j*xi*DUH
    call fftwl_execute_dft_c2r(planb, nablaDUH, nablaDU)
    U4=Dt*(nablaDU*1/N+alpha*Ut*(1-Ut/u0)-kappa*nabla4*1/N)
    u=u+U1/6+U2/3+U3/3+U4/6
    t=t+Dt
    if (int(k/skip)==k/skip) then
        write(*,*) minval(u),maxval(u), k
    endif
enddo

call cpu_time(finish)
print '("Time = ",f6.3," seconds.")',finish-start

END PROGRAM
