import FFTW
using AbstractFFTs 

const lb=0.27; kappa=10^-3; alpha=0.1; D0=1;u0=11

const L=140; N=1024;Dt=0.001;skip=20000;sigma=6

t=0
T=[0]

D(u::Array{Float64,1}) = D0*exp.(-lb*u).*(1 .-lb*u/2)

const x=collect(LinRange(-L/2,L/2,N))
const xi=2*pi/L*collect(UnitRange(0,Int64(N/2)))
const ixi= im*xi

u=1/sqrt.(2*pi*sigma.^2)*exp.(-x.^2/(2*sigma^2))
u*=1/maximum(u)*1.1*u0
const p=plan_rfft(u)
uhat=p*u
const q=plan_irfft(uhat,N)

const filtre = abs.(xi).<maximum(xi)*2/3

frames=[copy(u)]

function nabla(u::Array{Float64,1},uhat::Array{Complex{Float64},1})
    @fastmath @inbounds uhat=p*u
    @fastmath @inbounds uhat=ixi.*uhat
    return @fastmath @inbounds q*uhat
end
function nabla4(u::Array{Float64,1},uhat::Array{Complex{Float64},1})
    @fastmath @inbounds uhat=p*u
    @fastmath @inbounds uhat=ixi.^4 .*uhat
    return @fastmath @inbounds q*uhat
end

RKU(u::Array{Float64,1},uhat::Array{Complex{Float64},1})=nabla(D(u).*nabla(u,uhat),uhat).+alpha*u.*(1 .-u/u0).-kappa*nabla4(u,uhat)
#RKU(u::Array{Float64,1},uhat)=@. nabla(D(u)*nabla(u,uhat),uhat)+alpha*u*(1-u/u0)-kappa*nabla4(u,uhat)
function run(u::Array{Float64,1},uhat::Array{Complex{Float64},1},t)
    @fastmath @inbounds for k=0:5*10^5
        U1=Dt*RKU(u,uhat)
        U2=Dt*RKU(u.+U1/2,uhat)
        U3=Dt*RKU(u.+U2/2,uhat)
        U4=Dt*RKU(u.+U3,uhat)
        u+=@. U1/6+U2/3+U3/3+U4/6
        @fastmath @inbounds u=q*((p*u).*filtre)
        @fastmath @inbounds u=u.*(u.>10^-8)
        t+=Dt
        if div(k,skip)==k/skip
            #m=minimum(u) ; M=maximum(u)
            #println("min $m, max $M  it $k")
            println(minimum(u)," ",maximum(u))
            #println(maximum(u))
            #println(k)
        end
    end
    return u #,t
end

